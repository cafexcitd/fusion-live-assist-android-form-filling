    PDFJS.disableWorker = false;

    var pdfDoc = null,
        pageNum = 1,
        scale = 2,
        canvas = document.getElementById('the-canvas'),
        ctx = canvas.getContext('2d'),
        androidCallbacks = null,
        currentlyRendering = false,
        lastRenderTask = null;

    //
    // Get page info from document, resize canvas accordingly, and render page
    //
    function renderPage(num) {
      // Using promise to fetch the page
      pdfDoc.getPage(num).then(function(page) {
    	console.log("Page fetched");
    	if (androidCallbacks) {
    		androidCallbacks.onRenderingPage(num, pdfDoc.numPages);
    	}  
        var viewport = page.getViewport(scale);
        canvas.height = viewport.height;
        canvas.width = viewport.width;

        // Render PDF page into canvas context
        var renderContext = {
          canvasContext: ctx,
          viewport: viewport
        };
        //Ensure the page hasnt changed again since we obtained it.
        //If it has then don't render
        if (pageNum == num) {
	       console.log("Rendering page");
	       lastRenderTask = page.render(renderContext);
	       currentlyRendering = true;
	        lastRenderTask.promise.then(function pdfPageRenderCallback(){
	        	currentlyRendering = false;
	        	console.log("Page rendered");
	        	if (androidCallbacks) {
	        		androidCallbacks.onPageRendered(num, pdfDoc.numPages);
	        	} 
	        }, function pdfPageRenderError(error) {
	        	currentlyRendering = false;
	        });
        }
      });
    }

    //
    // Go to previous page
    //
    function goPrevious() {
      if (pageNum <= 1)
        return;
      
      cancelRenderingIfRequired();
      pageNum--;
      window.scrollTo(0, 0);
      renderPage(pageNum);
    }

    //
    // Go to next page
    //
    function goNext() {
      if (pageNum >= pdfDoc.numPages)
        return;
      
      cancelRenderingIfRequired();
      pageNum++;
      window.scrollTo(0, 0);
      renderPage(pageNum);
    }
    
    function cancelRenderingIfRequired() {
    	if (currentlyRendering == true && lastRenderTask) {
    		console.log("Cancelling page rendering");
    		lastRenderTask.internalRenderTask.cancel();
    	}
    }

    //
    // Asynchronously download PDF as an ArrayBuffer
    //
    function displayPdf(data) {
      console.log("displayPdf");
      PDFJS.getDocument(Base64Binary.decodeArrayBuffer(data)).then(function getPdfHelloWorld(_pdfDoc) {
        pdfDoc = _pdfDoc;
        renderPage(pageNum);
      });
    }