    //
    // Disable workers to avoid yet another cross-origin issue (workers need the URL of
    // the script to be loaded, and currently do not allow cross-origin scripts)
    //
    PDFJS.disableWorker = false;

    var pdfDoc = null;

    var element;
    var pageCanvas;
    var docFrame;

    function displayPdf(data) {
        console.log("in displayPdf");
    	//
        // Asynchronously download PDF as an ArrayBuffer
        //
        PDFJS.getDocument(Base64Binary.decodeArrayBuffer(data)).then(function(_pdfDoc) {
          pdfDoc = _pdfDoc;
          renderPages();
        });
    }
    
    function renderPages() {
    	element = document.getElementById('pdf');
	    pageCanvas = element.ownerDocument.createElement("canvas");
	    
    	docFrame = element.ownerDocument.createElement("div");
        docFrame.classList.add("document-frame");
        element.appendChild(docFrame);
        
        getAndRenderPages(pdfDoc, 1);
    }

    		
    function getAndRenderPages(pdfDoc, currentPageNumber) {
        console.log("in getAndRenderPages");
	
    	pdfDoc.getPage(currentPageNumber).then(function(page) {
		console.log(currentPageNumber);

		var pageImg = element.ownerDocument.createElement("img");

        	docFrame.appendChild(pageImg);
    		renderPage(page, pageCanvas, function() {
                var nextPage = currentPageNumber + 1;
		        console.log(nextPage);
		        pageImg.src = pageCanvas.toDataURL("image/png");
		        if (pdfDoc.numPages >= nextPage) {
			        getAndRenderPages(pdfDoc, nextPage);
		        }
		        else {
		            // callback to java
		            console.log("onPdfRendered() calling back");
		            if (PdfCallBack !== undefined || PdfCallBack !== null) {
		                PdfCallBack.onPdfRendered();
		            }
		        } 
	        });
    	});
    }
    
    //
    // Get page info from document, resize canvas accordingly, and render page
    //
    function renderPage(page, canvas, renderFinishedCallback) {
        var viewport = page.getViewport(2);
        canvas.height = viewport.height;
        canvas.width = viewport.width;

        var context = canvas.getContext("2d");
        // Render PDF page into canvas context
        var renderContext = {
          canvasContext: context,
          viewport: viewport
        };

        var renderTask = page.render(renderContext);
        var completeCallback = renderTask.internalRenderTask.callback;
        renderTask.internalRenderTask.callback = function (error) {
            completeCallback.call(this, error);
            renderFinishedCallback();
        }
    }

    //
    // Go to previous page
    //
    function goPrevious() {
      if (pageNum <= 1)
        return;
      pageNum--;
      renderPage(pageNum);
    }

    //
    // Go to next page
    //
    function goNext() {
      if (pageNum >= pdfDoc.numPages)
        return;
      pageNum++;
      renderPage(pageNum);
    }